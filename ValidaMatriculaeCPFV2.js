const fetch = require("node-fetch"),
  { URLSearchParams } = require("url");

const ids = [3621, 2881, 2562, 2561, 3401, 3261, 3441, 3461, 3481, 3482, 3521, 3541, 3601, 3641, 3662]; 

exports.handler = function (context, event, callback) {
  const response = {
    isUser: "false",
    Fila: -1,
  };

  validInputs(event)
    .then((inp) =>
      fetch(`${context.API_DOMAIN}/beneficiario/matriculaoucpf`, {
        method: "POST",
        headers: { "x-api-key": context.API_KEY },
        body: new URLSearchParams(`matriculaOuCpf=${inp.id}`),
      })
    )
    .then((res) => res.json())
    .then((res) => {
      if (!res.matricula) return;

      console.log(`Founded User ${res.matricula}`);

      response.isUser = "true";
      response.registration = res.matricula;
      response.name = res.nome;

      return fetch(`${context.API_DOMAIN}/carteira/matricula`, {
        method: "POST",
        headers: { "x-api-key": context.API_KEY },
        body: new URLSearchParams(`matricula=${res.matricula}`),
      });
    })
    .then((res) => res.json())
    .then((res) => {
      console.log("Finding queue");
      for (let obj of res) {
        for (let i = response.Fila + 1; i < ids.length; i++) {
          if (obj.id == ids[i]) {
            response.Fila = i;
            break;
          }
          if (response.Fila == ids.length) break;
        }
        console.log(`Queue`, response.Fila);
      }
      return;
    })
    .catch((err) => {
      if (err.type != "invalid-json") {
        console.log(err);
        console.log(`Error: ${JSON.stringify(err)}`);
      }
    })
    .finally(() => callback(null, response));
};

function validInputs(input) {
  return new Promise((resolve, reject) => {
    try {
      let { matriculaCPF } = input;

      console.log("Input", matriculaCPF);

      if (!matriculaCPF) throw "Blank matriculaCPF";

      matriculaCPF =
        matriculaCPF.length === 6 ? `0${matriculaCPF}` : matriculaCPF;

      if (matriculaCPF.length != 7 && matriculaCPF.length != 11)
        throw "Invalid matriculaCPF";

      resolve({
        id:
          matriculaCPF.length == 7
            ? `${matriculaCPF.toString().slice(0, -1)}-${
                matriculaCPF.toString()[6]
              }`
            : matriculaCPF,
      });
    } catch (err) {
      reject(err);
    }
  });
}
